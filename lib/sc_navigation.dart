import 'package:flutter/material.dart';
import 'package:flutter_listview/drawer/app_drawer.dart';
import 'package:flutter_listview/store.dart';
import 'package:flutter_listview/widget_fab_bottom_nav.dart';

import 'home.dart';

class TabNavigatorRoutes {
//  static const String root = '/';
  static const String home = '/home';
  static const String menu = '/menu';
  static const String notification = '/notification';
  static const String account = '/account';
}

class NavigationScreen extends StatefulWidget {
  NavigationScreen();

  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  List<FABBottomNavItem> _navMenus = List();
  PageController _pageController;
  int _selectedIndex = 0;
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  bool appbar = true;
  int id;

  // CAN CO DU LIEU ;;

  // _NavigationScreenState({this.type});

  @override
  void initState() {
    _pageController =
        new PageController(initialPage: _selectedIndex, keepPage: true);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _navMenus = _getTab();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _drawerKey,
        // appBar: appbar
        //     ? AppBar(
        //         backgroundColor: Colors.blue,
        //         actions: <IconButton>[
        //           IconButton(
        //             icon: Icon(Icons.search),
        //             onPressed: () {
        //               // AppNavigator.navigateSearch();
        //             },
        //           ),
        //         ],
        //       )
        //     : null,
        bottomNavigationBar: WidgetFABBottomNav(
          backgroundColor: Colors.red,
          selectedIndex: _selectedIndex,
          onTabSelected: (index) async {
            goToPage(page: index);
          },
          items: _navMenus,
          selectedColor: Colors.white,
          color: Colors.grey,
        ),
        drawer: AppDrawer(
          drawer: _drawerKey,
          menu: goToPage,
          notification: goToPage,
          account: goToPage,
        ),
        body: Column(
          children: [
            Container(
              child: Stack(
                children: [
                  Container(
                    color: Colors.red,
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.13,
                  ),
                  Positioned.fill(
                    top: MediaQuery.of(context).size.height * 0.13 / 4,
                    left: 10,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 1,
                          child: GestureDetector(
                            onTap: () {
                              _drawerKey.currentState.openDrawer();
                            },
                            child: Image.asset(
                              'assets/images/menu.png',
                              width: 30,
                              height: 30,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Expanded(
                          flex: 8,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(400),
                            child: Container(
                              height: 40,
                              color: Colors.white,
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Padding(
                                  padding: EdgeInsets.only(left: 15),
                                  child: Text(
                                    'Tìm sản phẩm',
                                    style: TextStyle(
                                      color: Colors.grey,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          flex: 1,
                          child: Image.asset(
                            'assets/images/bell.png',
                            width: 30,
                            height: 30,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          flex: 1,
                          child: Image.asset(
                            'assets/images/shopping.png',
                            width: 30,
                            height: 30,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
                child: PageView(
              controller: _pageController,
              physics: NeverScrollableScrollPhysics(),
              onPageChanged: (newPage) {
                setState(() {
                  this._selectedIndex = newPage;
                });
              },
              children: [
                MyHomePage(
                  onCategoryClick: ( id) {
                    goToPage(page: 2, id: id);
                    print("menu id click $id");
                     id = id;
                  },
                ),
                StorePage(
                  type: id

                ),
                MyHomePage(),
                MyHomePage(),
                MyHomePage(),
              ],
            ))
          ],
        ));
  }

  List<FABBottomNavItem> _getTab() {
    return List.from([
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.home,
          tabItem: TabItem.home,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/home.png',
          text: "Home"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.menu,
          tabItem: TabItem.menu,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/home.png',
          text: "Menu"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.notification,
          tabItem: TabItem.notification,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/home.png',
          text: "Notifications"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.account,
          tabItem: TabItem.account,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/home.png',
          text: "Acount"),
    ]);
  }

  void goToPage({int page, int id = 0}) {
    if (page != _selectedIndex) {
      setState(() {
        this._selectedIndex = page;
      });
      _pageController.jumpToPage(_selectedIndex);
    }
    setState(() {
      this._selectedIndex = page;
    });
    _pageController.jumpToPage(_selectedIndex);
  }
}
