import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'all_product.dart';
import 'list_avatar.dart';
import 'list_images.dart';

class MyHomePage extends StatefulWidget {
  @override
  final int id;
  final Function( int) onCategoryClick;
  MyHomePage({Key key, this.id, this.onCategoryClick}) : super(key: key);


  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          // Column(
          //   children: [
          //     Container(
          //       child: Stack(
          //         children: [
          //           Container(
          //             width: MediaQuery.of(context).size.width * 1,
          //             height: MediaQuery.of(context).size.width * 0.20,
          //             child: Container(
          //               color: Colors.blue,
          //             ),
          //           ),
          //           Positioned.fill(
          //             top: 20,
          //             left: 20,
          //             child: Row(
          //               mainAxisAlignment: MainAxisAlignment.start,
          //               children: [
          //                 Icon(
          //                   Icons.arrow_left,
          //                   color: Colors.white,
          //                 )
          //               ],
          //             ),
          //           ),
          //           Positioned.fill(
          //               child: FractionallySizedBox(
          //                 widthFactor: .5,
          //                 child: Padding(
          //                   padding: EdgeInsets.only(top: 20),
          //                   child: Align(
          //                       alignment: Alignment.center,
          //                       child: Text(
          //                         "Thông tin sự kiện",
          //                         style:
          //                         TextStyle(color: Colors.white, fontSize: 16),
          //                       )),
          //                 ),
          //               )),
          //         ],
          //       ),
          //     ),
          //     Divider(
          //       height: 1,
          //       thickness: 1,
          //       color: Colors.grey,
          //     ),
          //     SizedBox(
          //       height: 20,
          //     )
          //   ],
          // ),
          Expanded(
            flex: 4,
            child: GridView.builder(
                shrinkWrap: true,
                padding: EdgeInsets.only(top: 10, left: 5, right: 5),
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    child: ChildChooseImage(
                      id: index,
                    ),
                  );
                },
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                ),
                physics: NeverScrollableScrollPhysics(),
                itemCount: productAll.length),
          ),
          Expanded(
              flex: 2,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: productAll.length + 1,
                  itemBuilder: (context, index) {
                    if (index == 0) {
                      return GestureDetector(
                        onTap: null,
                        child: Column(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: Image.asset(
                                "assets/images/nh.png",
                                height: 60,
                                width: 60,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "name",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14),
                            )
                          ],
                        ),
                      );
                    }
                    return Container(
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      child: ChildImage(
                        onCategoryClick: ( id) {
                          this.widget.onCategoryClick(id);
                        },

                        id: index - 1,
                      ),
                    );
                  },
                ),
              )),
          Expanded(
              flex: 5,
              child: ListView.builder(
                itemCount: productAll.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    child: ChildChooseImage(
                      id: index,
                    ),
                  );
                },
              )),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
