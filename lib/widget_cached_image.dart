import 'package:flutter/material.dart';

class WidgetCachedImage extends StatelessWidget {
  WidgetCachedImage({this.url, this.color, this.fit});

  final String url;
  final Color color;
  final BoxFit fit;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Image.asset(
      url,
      color: color,
      fit: fit ?? BoxFit.fill,
      filterQuality: FilterQuality.low,
    ));
  }
}
