class Product {
  final String title,
      img,
      avatar,
      name,
      price,
      oldPrice,
      content,
      trademark,
      origin,
      date,
      code,
      count;
  final double star;
  final int like, quatity;

  Product(
      {this.title,
      this.img,
      this.avatar,
      this.name,
      this.price,
      this.oldPrice,
      this.content,
      this.trademark,
      this.origin,
      this.date,
      this.star,
      this.like,
      this.quatity,
      this.code,
      this.count});
}

List<Product> productAll = [
  Product(
      title: "Chương trình Sức khỏe Việt Nam Gia Lai",
      img: "assets/images/article.png",
      avatar: "assets/images/avatar.png",
      price: "339.000 đ",
      oldPrice: "600.000 đ",
      code: "678900",
      like: 0,
      origin: "hai phong",
      trademark: "china",
      date: "29-10-2020",
      content:
          "Bộ sản phẩm gồm có những bộ sản phẩm dưỡng trắng và mịn da, bộ sản phẩm trị mụn và trị thâm, bộ sản phẩm trị nám… Các sản phẩm đều được chiết xuất từ các thành phần thiên nhiên quen thuộc trong đời sống. Các nguyên liệu như da đam, lá rau má, các loại vitamin và khoáng chất có tác dụng làm trắng da, trị mờ vết thâm nám và trị mụn rất tốt.Bộ sản phẩm gồm có những bộ sản phẩm dưỡng trắng và mịn da, bộ sản phẩm trị mụn và trị thâm, bộ sản phẩm trị nám… Các sản phẩm đều được chiết xuất từ các thành phần thiên nhiên quen thuộc trong đời sống. Các nguyên liệu như da đam, lá rau má, các loại vitamin và khoáng chất có tác dụng làm trắng da, trị mờ vết thâm nám và trị mụn rất tốt.",
      star: 4,
      name: "hung",
      count: "50 %"),
  Product(
      title: "Chương trình Sức khỏe Việt Nam Gia Lai",
      img: "assets/images/article.png",
      avatar: "assets/images/avatar.png",
      price: "339.000 đ",
      oldPrice: "600.000 đ",
      code: "678900",
      like: 0,
      origin: "hai phong",
      trademark: "china",
      date: "29-10-2020",
      content:
          "Bộ sản phẩm gồm có những bộ sản phẩm dưỡng trắng và mịn da, bộ sản phẩm trị mụn và trị thâm, bộ sản phẩm trị nám… Các sản phẩm đều được chiết xuất từ các thành phần thiên nhiên quen thuộc trong đời sống. Các nguyên liệu như da đam, lá rau má, các loại vitamin và khoáng chất có tác dụng làm trắng da, trị mờ vết thâm nám và trị mụn rất tốt.",
      star: 4,
      name: "hung",
      count: "50 %"),
  Product(
      title: "Chương trình Sức khỏe Việt Nam Gia Lai",
      img: "assets/images/article.png",
      avatar: "assets/images/avatar.png",
      price: "339.000 đ",
      oldPrice: "600.000 đ",
      code: "678900",
      like: 0,
      origin: "hai phong",
      trademark: "china",
      date: "29-10-2020",
      content:
          "Bộ sản phẩm gồm có những bộ sản phẩm dưỡng trắng và mịn da, bộ sản phẩm trị mụn và trị thâm, bộ sản phẩm trị nám… Các sản phẩm đều được chiết xuất từ các thành phần thiên nhiên quen thuộc trong đời sống. Các nguyên liệu như da đam, lá rau má, các loại vitamin và khoáng chất có tác dụng làm trắng da, trị mờ vết thâm nám và trị mụn rất tốt.",
      star: 4,
      name: "hung",
      count: "50 %"),
  Product(
      title: "Chương trình Sức khỏe Việt Nam Gia Lai",
      img: "assets/images/article.png",
      avatar: "assets/images/avatar.png",
      price: "339.000 đ",
      oldPrice: "600.000 đ",
      code: "678900",
      like: 0,
      origin: "hai phong",
      trademark: "china",
      date: "29-10-2020",
      content:
          "Bộ sản phẩm gồm có những bộ sản phẩm dưỡng trắng và mịn da, bộ sản phẩm trị mụn và trị thâm, bộ sản phẩm trị nám… Các sản phẩm đều được chiết xuất từ các thành phần thiên nhiên quen thuộc trong đời sống. Các nguyên liệu như da đam, lá rau má, các loại vitamin và khoáng chất có tác dụng làm trắng da, trị mờ vết thâm nám và trị mụn rất tốt.",
      star: 4,
      name: "hung",
      count: "50 %"),
  Product(
      title: "Chương trình Sức khỏe Việt Nam Gia Lai",
      img: "assets/images/article.png",
      avatar: "assets/images/avatar.png",
      price: "339.000 đ",
      oldPrice: "600.000 đ",
      code: "678900",
      like: 0,
      origin: "hai phong",
      trademark: "china",
      date: "29-10-2020",
      content:
          "Bộ sản phẩm gồm có những bộ sản phẩm dưỡng trắng và mịn da, bộ sản phẩm trị mụn và trị thâm, bộ sản phẩm trị nám… Các sản phẩm đều được chiết xuất từ các thành phần thiên nhiên quen thuộc trong đời sống. Các nguyên liệu như da đam, lá rau má, các loại vitamin và khoáng chất có tác dụng làm trắng da, trị mờ vết thâm nám và trị mụn rất tốt.",
      star: 4,
      name: "hung",
      count: "50 %"),
  Product(
      title: "Chương trình Sức khỏe Việt Nam Gia Lai",
      img: "assets/images/article.png",
      avatar: "assets/images/avatar.png",
      price: "339.000 đ",
      oldPrice: "600.000 đ",
      code: "678900",
      like: 0,
      origin: "hai phong",
      trademark: "china",
      date: "29-10-2020",
      content:
          "Bộ sản phẩm gồm có những bộ sản phẩm dưỡng trắng và mịn da, bộ sản phẩm trị mụn và trị thâm, bộ sản phẩm trị nám… Các sản phẩm đều được chiết xuất từ các thành phần thiên nhiên quen thuộc trong đời sống. Các nguyên liệu như da đam, lá rau má, các loại vitamin và khoáng chất có tác dụng làm trắng da, trị mờ vết thâm nám và trị mụn rất tốt.",
      star: 4,
      name: "hung",
      count: "50 %"),
  Product(
      title: "Chương trình Sức khỏe Việt Nam Gia Lai",
      img: "assets/images/article.png",
      avatar: "assets/images/avatar.png",
      price: "339.000 đ",
      oldPrice: "600.000 đ",
      code: "678900",
      like: 0,
      origin: "hai phong",
      trademark: "china",
      date: "29-10-2020",
      content:
          "Bộ sản phẩm gồm có những bộ sản phẩm dưỡng trắng và mịn da, bộ sản phẩm trị mụn và trị thâm, bộ sản phẩm trị nám… Các sản phẩm đều được chiết xuất từ các thành phần thiên nhiên quen thuộc trong đời sống. Các nguyên liệu như da đam, lá rau má, các loại vitamin và khoáng chất có tác dụng làm trắng da, trị mờ vết thâm nám và trị mụn rất tốt.",
      star: 4,
      name: "hung",
      count: "50 %"),
  Product(
      title: "Chương trình Sức khỏe Việt Nam Gia Lai",
      img: "assets/images/test.png",
      avatar: "assets/images/avatar.png",
      price: "339.000 đ",
      oldPrice: "600.000 đ",
      code: "678900",
      like: 1,
      origin: "hai phong",
      trademark: "china",
      date: "29-10-2020",
      content:
          "Bộ sản phẩm gồm có những bộ sản phẩm dưỡng trắng và mịn da, bộ sản phẩm trị mụn và trị thâm, bộ sản phẩm trị nám… Các sản phẩm đều được chiết xuất từ các thành phần thiên nhiên quen thuộc trong đời sống. Các nguyên liệu như da đam, lá rau má, các loại vitamin và khoáng chất có tác dụng làm trắng da, trị mờ vết thâm nám và trị mụn rất tốt.",
      star: 4,
      name: "hung",
      count: "20 %"),
];
