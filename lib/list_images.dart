import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_listview/all_product.dart';

import 'detail_product_screen.dart';

class ChildChooseImage extends StatelessWidget {
  final int id;
  ChildChooseImage({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => DetailScreen(id: id)),
      ),
      child: Container(
        height: 120,
        child: Card(
          child: Row(
            children: [
              Expanded(
                  flex: 5,
                  child: Image.asset(
                    "${productAll[id].img}",
                    fit: BoxFit.cover,
                  )),
              Expanded(
                  flex: 5,
                  child: Container(
                    color: Colors.blue[200],
                    child: Center(
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        child: Text(
                          "${productAll[id].title}",
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
