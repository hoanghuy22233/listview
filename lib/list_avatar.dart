import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_listview/all_product.dart';

class ChildImage extends StatelessWidget {
  final int id;
  final Function( int) onCategoryClick;
  ChildImage({Key key, this.id, this.onCategoryClick}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
           onTap: (){
             onCategoryClick(id);
           },
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: Image.asset(
              "${productAll[id].avatar}",
              height: 60,
              width: 60,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "${productAll[id].name}",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
          )
        ],
      ),
    );
  }
}
