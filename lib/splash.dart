import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_listview/sc_navigation.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with AutomaticKeepAliveClientMixin<SplashScreen> {
  @override
  void initState() {
    super.initState();
    openLogin();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: false,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Image.asset(
                "assets/images/splash.png",
                fit: BoxFit.fill,
              ),
            ),
            Image.asset(
              "assets/images/logo.png",
              height: 150,
              width: 150,
            )
          ],
        ),
      ),
    );
  }

  void openLogin() async {
    //thoi gian delay
    Future.delayed(Duration(seconds: 3), () {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => NavigationScreen()));
    });
  }

  // void openLogin(UserRepository repository) async {
  //   try {
  //     final profileResponse = await repository.getProfile();
  //     if (profileResponse.status == Endpoint.SUCCESS) {
  //       final prefs = LocalPref();
  //       final token = await prefs.getString(AppPreferences.auth_token);
  //       BlocProvider.of<AuthenticationBloc>(Get.context).add(LoggedIn(token));
  //       //  AppNavigator.navigateNavigation();
  //     }
  //     // AppNavigator.navigateNavigation();
  //   } on DioError catch (e) {} finally {
  //     Future.delayed(Duration(milliseconds: 350), () {
  //       CircularProgressIndicator();
  //       AppNavigator.navigateNavigation();
  //     });
  //   }
  // }

  @override
  bool get wantKeepAlive => true;
}
