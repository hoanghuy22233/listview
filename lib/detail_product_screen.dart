import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

import 'all_product.dart';

class DetailScreen extends StatefulWidget {
  final int id;
  DetailScreen({Key key, this.id}) : super(key: key);
  _DetailScreenState createState() => new _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  bool isReadDetail = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            children: [
              Container(
                child: Stack(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 1,
                      height: MediaQuery.of(context).size.width * 0.20,
                      child: Container(
                        color: Colors.red,
                      ),
                    ),
                    Positioned.fill(
                      top: 20,
                      left: 20,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Image.asset(
                              "assets/images/left.png",
                              height: 25,
                              width: 25,
                            )
                          ],
                        ),
                      ),
                    ),
                    Positioned.fill(
                        child: FractionallySizedBox(
                      widthFactor: .5,
                      child: Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              "Thông tin sản phẩm",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            )),
                      ),
                    )),
                  ],
                ),
              ),
              Divider(
                height: 1,
                thickness: 1,
                color: Colors.grey,
              ),
            ],
          ),
          Expanded(
              child: ListView(
            padding: EdgeInsets.zero,
            scrollDirection: Axis.vertical,
            children: [
              Stack(
                alignment: Alignment.bottomRight,
                children: [
                  Container(
                    child: Image.asset(
                      "${productAll[widget.id].img}",
                      height: MediaQuery.of(context).size.height / 2,
                      width: MediaQuery.of(context).size.width,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 20, bottom: 20),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(500),
                      child: Container(
                        padding: EdgeInsets.all(10),
                        height: 40,
                        width: 40,
                        color: Colors.grey[300],
                        child: productAll[widget.id].like == 0
                            ? Image.asset(
                                "assets/images/heart.png",
                                height: 15,
                                width: 15,
                              )
                            : Image.asset(
                                "assets/images/favorites.png",
                                height: 15,
                                width: 15,
                              ),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Row(
                  children: [
                    Expanded(
                        child: Text(
                      "${productAll[widget.id].name}",
                      style: TextStyle(
                          color: Colors.red,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    )),
                    Text("Ms: ${productAll[widget.id].code}"),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  productAll[widget.id].title,
                  style: TextStyle(color: Colors.grey, fontSize: 16),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  children: [
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      productAll[widget.id].price,
                      style: TextStyle(
                          color: Colors.green,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      productAll[widget.id].oldPrice,
                      style: TextStyle(color: Colors.grey, fontSize: 12),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(2),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Container(
                            height: 20,
                            width: 40,
                            color: Colors.red,
                          ),
                          Text(
                            "- ${productAll[widget.id].count}",
                            style: TextStyle(color: Colors.grey, fontSize: 12),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Row(
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  SmoothStarRating(
                    rating: productAll[widget.id].star,
                    isReadOnly: false,
                    size: 25,
                    filledIconData: Icons.star,
                    halfFilledIconData: Icons.star_half,
                    defaultIconData: Icons.star_border,
                    starCount: 5,
                    borderColor: Colors.amber,
                    color: Colors.amber,
                    allowHalfRating: true,
                    spacing: 2.0,
                    onRated: (value) {
                      print("rating value -> $value");
                      // print("rating value dd -> ${value.truncate()}");
                    },
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(productAll[widget.id].star.toString())
                ],
              ),

              //padding text////
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                child: Container(
                  height: isReadDetail ? null : 50,
                  child: Text(
                    productAll[widget.id].content,
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              productAll[widget.id].content.length > 100
                  ? Column(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Center(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                isReadDetail = !isReadDetail;
                              });
                            },
                            child: isReadDetail
                                ? Text(
                                    "Thu gọn",
                                    style: TextStyle(color: Colors.blue),
                                  )
                                : Text(
                                    "Xem thêm",
                                    style: TextStyle(color: Colors.blue),
                                  ),
                          ),
                        ),
                      ],
                    )
                  : SizedBox(),
            ],
          ))
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
